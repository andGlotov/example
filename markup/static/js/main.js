import $ from 'static/js/libraries/jquery-3.3.1.min.js';
import promoSlider from 'components/promo-slider/promo-slider';
import popup from 'components/popup/popup';
import tabs from 'components/tabs/tabs';



$(document).ready(function () {
    promoSlider();
    popup();
    tabs();
});
