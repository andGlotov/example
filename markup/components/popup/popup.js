import $ from 'static/js/libraries/jquery-3.3.1.min.js';
import validator from 'validator';

export default function () {

var popup =  function () {
    setDefoult('#Autentification-form');
    $('#overlay').fadeIn(400,
        function () {
            $('#modal_form')
                .css('display', 'block')
                .animate({opacity: 1, top: '30%'}, 200);
        });


    $('#modal_close, #overlay').click(function () {
        $('#modal_form')
            .animate({opacity: 0, top: '45%'}, 200,
                function () {
                    $(this).css('display', 'none');
                    $('#overlay').fadeOut(400);
                }
            );
    });
}

var setDefoult = function (Form) {
    $(Form).find('input').css("border", "1px solid black");
    $(Form).find('.js-message-err').hide();
}


var validationForm = function () {
	    var emailVal = $('input[type="email"]').val(),
	    	email = $('input[type="email"]'),
	        passwordVal = $('input[name="pass"]').val(),
	        password = $('input[name="pass"]'),
	        password1Val = $('input[name="pass1"]').val(),
	        RePassword = $('input[name="pass1"]');


	    if ($('#swap-autentificaton').hasClass('swap')) {
	        //registred
	        if (validator.isEmail(emailVal)
	            && validator.isByteLength(passwordVal, {min: 6, max: undefined})
	            && !validator.isUppercase(passwordVal) && !validator.isLowercase(passwordVal) &&
	            validator.equals(passwordVal, password1Val)) {
	            email.css("border", "1px solid green");
	           	password.css("border", "1px solid green");
	            RePassword.css("border", "1px solid green");
	            return true;

	        } else {
	            if (validator.isEmail(emailVal)) {
	                email.css("border", "1px solid green");
	               	password.css("border", "1px solid red");
	                RePassword.css("border", "1px solid red");
	                return false;
	            } else {
	                if (!validator.isUppercase(passwordVal) && !validator.isLowercase(passwordVal) &&
	                    validator.equals(passwordVal, password1Val)) {
	                    email.css("border", "1px solid red");
	                    password.css("border", "1px solid green");
	                    RePassword.css("border", "1px solid green");
	                    return false;
	                } else {
	                    email.css("border", "1px solid red");
	                    password.css("border", "1px solid red");
	                    RePassword.css("border", "1px solid red");
	                    return false;
	                }

	            }
	        }

	    } else { // autentification
	        if (validator.isEmail(emailVal)
	            && validator.isByteLength(passwordVal, {min: 6, max: undefined})
	            && !validator.isUppercase(passwordVal) && !validator.isLowercase(passwordVal)) {
	            //okay
	            email.css("border", "1px solid green");
	            password.css("border", "1px solid green");
	            return true;

	        } else {
	            if (validator.isEmail(emailVal)) {
	                email.css("border", "1px solid green");
	                password.css("border", "1px solid red");
	                return false;
	            } else {
	                if (validator.isByteLength(passwordVal, {min: 6, max: undefined})
	                    && !validator.isUppercase(passwordVal) && !validator.isLowercase(passwordVal)) {
	                    email.css("border", "1px solid red");
	                    password.css("border", "1px solid green");
	                    return false;
	                } else {
	                    email.css("border", "1px solid red");
	                    password.css("border", "1px solid red");
	                    return false
	                }
	            }

	        }
	    }
	}


    $('a#poupup').click(function (event) {
        event.preventDefault();
        popup();
    });
    var erMessage = $('.js-message-err');
// validation
    $(document).on('focusout', '.Autentification-form__input', function () {
    	
        if (validationForm()) {
            erMessage.hide();
        } else {
            erMessage.show();
        }

    });

    $(document).on('click', '.enter-btn', function (e) {
        e.preventDefault();
        if (validationForm()) {
            //ajax
        } else {
            $('.js-message-err').show();
        }

    });


    $(document).on('click', '#swap-autentificaton', function (e) {
        e.preventDefault();
        setDefoult('#Autentification-form');
        $(this).toggleClass('swap');
        if ($(this).hasClass('swap')) { // registred
            $(this).text('Вернуться на страницу авторизации');
            $('input[name="pass1"]').show();
            $('#Autentification-form > h1').text('Регистрация');
            $('.enter-btn').val('Подтвердить данные')
            $('.js-rettryPassword').show();
            $('.js-message-err').html('Проверьте корректность введенного вами почтового ящика! Пароль должен содержать не менее 6 символов , хотябы 1 заглавную букву и  1 цифру! Пароли должны совпадать!')
        } else { // autentification
            $(this).text('У вас нет логина?');
            $('.js-rettryPassword').show();
            $('input[name="pass1"]').hide();
            $('#Autentification-form > h1').text('Авторизация');
            $('.enter-btn').val('Авторизоваться');
            $('.js-message-err').html('Проверьте корректность введенного вами почтового ящика! Пароль должен содержать не менее 6 символов , хотябы 1 заглавную букву и  1 цифру!')

        }
    });


}


