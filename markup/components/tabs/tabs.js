import $ from 'static/js/libraries/jquery-3.3.1.min.js';
   

export default function () {
   $(document).on('click', '.tabs-list__link', function (event) {
        event.preventDefault();

        $('.tabs-list__link').each(function (index) {
            $(this).removeClass('is-active');
        });

        $(this).addClass('is-active');

        var navBtnId = $(this).parent().index();
        var tabItem = $('.tabs-content__item')
        if (navBtnId) {
            tabItem.hide();
            tabItem.eq(navBtnId).fadeIn();
        } else {
            tabItem.hide();
            tabItem.eq(0).fadeIn();
        }

    })

}