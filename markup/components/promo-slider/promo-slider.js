import swiper from 'static/js/libraries/swiper.min.js';

export default function () {
    new swiper('.swiper-container', {
        loop: true,
        speed: 800,
        navigation: {
            nextEl: '.promo-slider__btn--next',
            prevEl: '.promo-slider__btn--prev',
        },
        effect: 'fade',
        autoplay: {
            delay: 5000,
        },
        fadeEffect: {
            crossFade: true
        },

    });
}
